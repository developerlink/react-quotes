import React from "react";
import { Routes, Route, Link, useParams, Outlet } from "react-router-dom";
import HighlightedQuote from "../quotes/HighlightedQuote";
import Comments from "../comments/Comments";
import useHttp from "../hooks/use-http";
import { getSingleQuote } from "../lib/api";
import { useEffect } from "react/cjs/react.development";
import LoadingSpinner from "../UI/LoadingSpinner";

const QuoteDetail = () => {
  const params = useParams();
  const {quoteId} = params;
  const { sendRequest, status, data: loadedQuote, error } = useHttp(getSingleQuote, true);

  useEffect(() => {
    sendRequest(quoteId); 
  }, [sendRequest, quoteId])

  if (status === 'pending') {
    return <div className='centered'><LoadingSpinner /></div>
  }

  if (error) {
    return <p className='centered'>{error}</p>
  }

  if (!loadedQuote.text) {
    return <p>No quote found!</p>;
  }

  return (
    <section>
      <HighlightedQuote text={loadedQuote.text} author={loadedQuote.author} />
      <Routes>
        <Route path="" element={<div className="centered"><Link to="comments">Load Comments</Link></div>}/>
        <Route path="comments" element={<Comments />} />
      </Routes>
      <Outlet />
    </section>
  );
};

export default QuoteDetail;
